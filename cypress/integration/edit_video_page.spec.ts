describe('Edit video page', () => {
  beforeEach(() => {
    cy.request('POST', Cypress.env('apiUrl') + '/categories', {
      id: 996,
      name: 'Integ sample category 1',
    });
    cy.request('POST', Cypress.env('apiUrl') + '/categories', {
      id: 995,
      name: 'Integ sample category 2',
    });
    cy.request('POST', Cypress.env('apiUrl') + '/authors', {
      id: 996,
      name: 'Integ sample author 1',
      videos: [
        {
          id: 996,
          catIds: [996],
          name: 'Integ sample video',
        },
      ],
    });
    cy.request('POST', Cypress.env('apiUrl') + '/authors', {
      id: 995,
      name: 'Integ sample author 2',
      videos: [],
    });
  });

  afterEach(() => {
    cy.request('DELETE', Cypress.env('apiUrl') + '/authors/996');
    cy.request('DELETE', Cypress.env('apiUrl') + '/authors/995');
    cy.request('DELETE', Cypress.env('apiUrl') + '/categories/995');
    cy.request('DELETE', Cypress.env('apiUrl') + '/categories/996');
  });

  it('loads successfully', () => {
    cy.visit('/videos/996');
    cy.get('h2').should('contain', 'Edit video: Integ sample video');
  });

  it('edits video', () => {
    cy.visit('/videos/996');

    cy.get('[data-testid=name]').type('{selectall}').type('Integ sample video 2');

    cy.get('[data-testid=author]').click();
    cy.get('[role=option]').contains('Integ sample author 2').click();

    cy.get('[data-testid=categories]').click();
    cy.get('[role=option]').contains('Integ sample category 1').click();
    cy.get('[role=option]').contains('Integ sample category 2').click().type('{esc}');

    cy.get('[data-testid=submit]').click();

    cy.location('pathname').should('equal', '/');
    cy.get('tbody tr')
      .contains('tr', 'Integ sample video 2')
      .should('contain', 'Integ sample author 2')
      .should('contain', 'Integ sample category 2');
  });

  it('cancels editing video', () => {
    cy.visit('/videos/996');

    cy.get('[data-testid=name]').type('{selectall}').type('Integ sample video 2');

    cy.get('[data-testid=author]').click();
    cy.get('[role=option]').contains('Integ sample author 2').click();

    cy.get('[data-testid=categories]').click();
    cy.get('[role=option]').contains('Integ sample category 1').click();
    cy.get('[role=option]').contains('Integ sample category 2').click().type('{esc}');

    cy.get('[data-testid=cancel]').click();

    cy.location('pathname').should('equal', '/');
    cy.get('tbody tr').should('not.contain', 'Integ sample video 2');
  });
});
