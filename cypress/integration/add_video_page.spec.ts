describe('Add video page', () => {
  beforeEach(() => {
    cy.request('POST', Cypress.env('apiUrl') + '/authors', {
      id: 997,
      name: 'Integ sample author',
      videos: [],
    });
    cy.request('POST', Cypress.env('apiUrl') + '/categories', {
      id: 999,
      name: 'Integ sample category',
    });
  });

  afterEach(() => {
    cy.request('DELETE', Cypress.env('apiUrl') + '/authors/997');
    cy.request('DELETE', Cypress.env('apiUrl') + '/categories/999');
  });

  it('adds new video', () => {
    cy.visit('/videos/new');

    cy.get('[data-testid=name]').type('Integ sample video');

    cy.get('[data-testid=author]').click();
    cy.get('[role=option]').contains('Integ sample author').click();

    cy.get('[data-testid=categories]').click();
    cy.get('[role=option]').contains('Integ sample category').click().type('{esc}');

    cy.get('[data-testid=submit]').click();

    cy.location('pathname').should('equal', '/');
    cy.get('tbody tr').should('contain', 'Integ sample video');

    cy.get('tbody tr')
      .contains('tr', 'Integ sample video')
      .should('contain', 'Integ sample author')
      .should('contain', 'Integ sample category')
      .should('contain', 'one 1080p');
  });

  it('cancels adding new video', () => {
    cy.visit('/videos/new');

    cy.get('[data-testid=name]').type('Integ sample video');

    cy.get('[data-testid=author]').click();
    cy.get('[role=option]').contains('Integ sample author').click();

    cy.get('[data-testid=categories]').click();
    cy.get('[role=option]').contains('Integ sample category').click().type('{esc}');

    cy.get('[data-testid=cancel]').click();

    cy.location('pathname').should('equal', '/');
    cy.get('tbody tr').should('not.contain', 'Integ sample video');
  });
});
