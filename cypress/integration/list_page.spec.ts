describe('List page', () => {
  before(() => {
    cy.request('POST', Cypress.env('apiUrl') + '/authors', {
      id: 999,
      name: 'Integ sample author',
      videos: [
        {
          id: 999,
          catIds: [1, 2],
          name: 'Integ sample video',
        },
      ],
    });
  });

  after(() => {
    cy.request('DELETE', Cypress.env('apiUrl') + '/authors/999');
  });

  it('successfully loads', () => {
    cy.visit('/');
    cy.get('header').should('contain', 'Video manager');
  });

  it('lists videos', () => {
    cy.visit('/');

    cy.get('tbody tr').should('contain', 'Integ sample video');
  });

  it('searches videos', () => {
    cy.visit('/');

    cy.get('tbody').should('contain', 'Integ sample video');
    cy.get('[data-testid="search"]').type('AAA');
    cy.get('tbody').should('not.contain', 'Integ sample video');
  });

  it('navigates to add video', () => {
    cy.visit('/');
    cy.get('[data-testid="add-video"]').click();
    cy.location('pathname').should('equal', '/videos/new');
  });

  it('cancels video deletion', () => {
    cy.visit('/');
    cy.get('tbody tr').should('contain', 'Integ sample video');
    cy.get('tbody tr').contains('tr', 'Integ sample video').find('button[data-testid="delete-video"]').click();
    cy.get('[data-testid="delete-dialog"]').should('be.visible');
    cy.get('[data-testid="cancel-delete"').click();
    cy.get('[data-testid="delete-dialog"]').should('not.exist');
    cy.get('tbody tr').should('contain', 'Integ sample video');
  });

  it('deletes video', () => {
    cy.visit('/');
    cy.get('tbody tr').should('contain', 'Integ sample video');
    cy.get('tbody tr').contains('tr', 'Integ sample video').find('button[data-testid="delete-video"]').click();
    cy.get('[data-testid="delete-dialog"]').should('be.visible');
    cy.get('[data-testid="confirm-delete"').click();
    cy.get('tbody tr').should('not.contain', 'Integ sample video');
  });

  it('refreshes list', () => {
    cy.visit('/');
    cy.get('tbody tr').should('not.contain', 'Integ new sample video');

    cy.request('POST', Cypress.env('apiUrl') + '/authors', {
      id: 998,
      name: 'Integ new sample author',
      videos: [
        {
          id: 998,
          catIds: [1, 2],
          name: 'Integ new sample video',
        },
      ],
    });

    cy.get('[data-testid="refresh-list"]').click();

    cy.get('tbody tr').should('contain', 'Integ new sample video');

    cy.request('DELETE', Cypress.env('apiUrl') + '/authors/998');
  });
});
