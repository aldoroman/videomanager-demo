import { act, fireEvent, getByTestId, render } from '@testing-library/react';
import React from 'react';
import { VideoForm } from './video-form';

jest.mock('../services/categories', () => ({
  __esModule: true,
  getCategories: () =>
    Promise.resolve([
      { id: 1, name: 'category 1' },
      { id: 2, name: 'category 2' },
    ]),
}));

jest.mock('../services/authors', () => ({
  __esModule: true,
  getAuthors: () =>
    Promise.resolve([
      {
        id: 0,
        name: 'test author name',
        videos: [
          {
            id: 0,
            name: 'test video name',
            catIds: [1],
          },
        ],
      },
    ]),
}));

const defaultProps = {
  video: {
    id: 0,
    name: 'test video name',
    author: 'test author name',
    categories: ['category 1', 'category 2'],
  },
  onChange: jest.fn(),
  onSubmit: jest.fn(),
  onCancel: jest.fn(),
};

afterEach(() => {
  jest.resetAllMocks();
});

describe('VideoForm', () => {
  it('displays right data', async () => {
    const container = document.createElement('div');

    await act(async () => {
      render(<VideoForm {...defaultProps} />, { container });
    });

    const nameInput = getByTestId(container, 'name').querySelector('input');
    expect(nameInput?.value).toEqual('test video name');

    const authorSelect = getByTestId(container, 'author').querySelector('input');
    expect(authorSelect?.value).toEqual('test author name');

    const categorySelect = getByTestId(container, 'categories').querySelector('input');
    expect(categorySelect?.value).toEqual('category 1,category 2');
  });

  it('calls onChange', async () => {
    await act(async () => {
      render(<VideoForm {...defaultProps} />);
    });

    const nameInput = getByTestId(document.body, 'name').querySelector('input')!;
    fireEvent['change'](nameInput, { target: { value: 'another video test name' } });
    
    expect(defaultProps.onChange).toHaveBeenCalledTimes(1);
    expect(defaultProps.onChange).toHaveBeenCalledWith({
      id: 0,
      name: 'another video test name',
      author: 'test author name',
      categories: ['category 1', 'category 2'],
    });
  });

  it('cancels form', async () => {
    await act(async () => {
      render(<VideoForm {...defaultProps} />);
    });

    const cancelButton = getByTestId(document.body, 'cancel');
    cancelButton.click();
    expect(defaultProps.onCancel).toHaveBeenCalled();
  });

  it('submits form', async () => {
    await act(async () => {
      render(<VideoForm {...defaultProps} />);
    });

    const submitButton = getByTestId(document.body, 'submit');
    submitButton.click();
    expect(defaultProps.onSubmit).toHaveBeenCalled();
  });
});
