import { render } from '@testing-library/react';
import React from 'react';
import { DeleteDialog } from './delete-dialog';

const defaultProps = {
  open: true,
  video: {
    id: 1,
    name: 'test video name',
    author: 'test author name',
    categories: ['test category 1', 'test category 2'],
  },
  onDelete: jest.fn(),
  onCancel: jest.fn(),
};

afterEach(() => {
  jest.resetAllMocks();
});

describe('DeleteDialog', () => {
  it('does not display without video', () => {
    const { queryByTestId } = render(<DeleteDialog {...defaultProps} video={null} />);
    const element = queryByTestId('delete-dialog');
    expect(element).toBeNull();
  });

  it('does not display when not open', () => {
    const { queryByTestId } = render(<DeleteDialog {...defaultProps} open={false} />);
    const element = queryByTestId('delete-dialog');
    expect(element).toBeNull();
  });

  it('displays right data', () => {
    const { getByTestId } = render(<DeleteDialog {...defaultProps} />);
    const element = getByTestId('delete-dialog');
    expect(element).not.toBeNull();
    expect(element.textContent).toContain('test video name');
    expect(element.textContent).toContain('test author name');
  });

  it('cancels dialog', () => {
    const { getByTestId } = render(<DeleteDialog {...defaultProps} />);
    const cancelButton = getByTestId('cancel-delete');
    cancelButton.click();
    expect(defaultProps.onCancel).toHaveBeenCalledTimes(1);
    expect(defaultProps.onDelete).not.toHaveBeenCalled();
  });

  it('confirms dialog', () => {
    const { getByTestId } = render(<DeleteDialog {...defaultProps} />);
    const confirmButton = getByTestId('confirm-delete');
    confirmButton.click();
    expect(defaultProps.onCancel).not.toHaveBeenCalled();
    expect(defaultProps.onDelete).toHaveBeenCalledTimes(1);
  });
});
