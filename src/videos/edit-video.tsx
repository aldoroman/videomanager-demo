import React, { useCallback, useEffect, useState } from 'react';
import { useHistory, useParams } from 'react-router-dom';
import { VideoForm } from './components/video-form';
import { ProcessedVideo } from './interfaces';
import { getVideo, saveVideo } from './services/videos';

interface EditVideoParams {
  id: string;
}

export const EditVideo: React.FC = () => {
  const { id } = useParams<EditVideoParams>();
  const history = useHistory();

  const [loading, setLoading] = useState<boolean>(true);
  const [video, setVideo] = useState<ProcessedVideo | null>(null);

  useEffect(() => {
    getVideo(Number.parseInt(id))
      .then((video) => {
        setVideo(video);
      })
      .finally(() => setLoading(false));
  }, [id]);

  const onVideoChange = useCallback((video: ProcessedVideo) => {
    setVideo(video);
  }, []);

  const onSubmit = useCallback(async () => {
    // validate
    // send video to API
    if (video) {
        await saveVideo(video);
        // navigate to list
        history.push('/');
    }
    // TODO error handling
  }, [history, video]);

  const onCancel = useCallback(() => {
    // navigate to list
    history.push('/');
  }, [history]);

  if (loading) {
    return <p>Loading...</p>;
  }

  if (video === null) {
    return <p>Error while loading video with ID: {id}</p>;
  }

  return (
    <>
      <h2>Edit video: {video.name}</h2>
      <VideoForm video={video} onChange={onVideoChange} onSubmit={onSubmit} onCancel={onCancel} />
    </>
  );
};
