import { Button, Paper, Table, TableBody, TableCell, TableContainer, TableHead, TableRow } from '@material-ui/core';
import React from 'react';
import { Link } from 'react-router-dom';
import { ProcessedVideo } from './interfaces';
import { getFormatLabel as getHighestFormatLabel } from './services/videos';

interface VideosTableProps {
  videos: ProcessedVideo[];
  onDeleteVideo: (video: ProcessedVideo) => void;
}
// TODO make sortable: https://material-ui.com/components/tables/#sorting-amp-selecting
export const VideosTable: React.FC<VideosTableProps> = ({ videos, onDeleteVideo }) => {
  return (
    <TableContainer component={Paper}>
      <Table>
        <TableHead>
          <TableRow>
            <TableCell>Video Name</TableCell>
            <TableCell>Author</TableCell>
            <TableCell>Categories</TableCell>
            <TableCell>Highest quality format</TableCell>
            <TableCell>Actions</TableCell>
          </TableRow>
        </TableHead>
        <TableBody>
          {videos.map((video) => (
            <TableRow key={video.id}>
              <TableCell component="th" scope="row">
                {video.name}
              </TableCell>
              <TableCell>{video.author}</TableCell>
              <TableCell>{video.categories.join(', ')}</TableCell>
              <TableCell>{video.formats ? getHighestFormatLabel(video.formats) : '-'}</TableCell>
              <TableCell>
                <Button component={Link} to={`/videos/${video.id}`}>
                  Edit
                </Button>
                <Button color="secondary" onClick={onDeleteVideo.bind(null, video)} data-testid="delete-video">
                  Delete
                </Button>
              </TableCell>
            </TableRow>
          ))}
        </TableBody>
      </Table>
    </TableContainer>
  );
};
