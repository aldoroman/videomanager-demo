import React, { useCallback, useState } from 'react';
import { useHistory } from 'react-router-dom';
import { VideoForm } from './components/video-form';
import { ProcessedVideo } from './interfaces';
import { saveVideo } from './services/videos';

export const AddVideo: React.FC = () => {
  const history = useHistory();

  const [video, setVideo] = useState<ProcessedVideo | undefined>(undefined);

  const onVideoChange = useCallback((video: ProcessedVideo) => {
    setVideo(video);
  }, []);

  const onSubmit = useCallback(async () => {
    // validate
    
    // send video to API
    if (video){
      await saveVideo(video);
      // navigate to list
      history.push('/');
    }
  }, [history, video]);

  const onCancel = useCallback(() => {
    // navigate to list
    history.push('/');
  }, [history]);

  return (
    <>
      <h2>Add video</h2>
      <VideoForm video={video} onChange={onVideoChange} onSubmit={onSubmit} onCancel={onCancel} />
    </>
  );
};
