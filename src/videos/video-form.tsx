import { Button, Grid, MenuItem, TextField } from '@material-ui/core';
import React, { useCallback, useEffect, useState } from 'react';
import { Author, Category, ProcessedVideo } from './interfaces';
import { getAuthors } from './services/authors';
import { getCategories } from './services/categories';

interface VideoFormProps {
  video?: ProcessedVideo;
  onChange: (video: ProcessedVideo) => void;
  onSubmit: () => void;
  onCancel: () => void;
}

const emptyVideo: ProcessedVideo = { name: '', id: NaN, author: '', categories: [] };

export const VideoForm: React.FC<VideoFormProps> = ({ video = emptyVideo, onChange, onSubmit, onCancel }) => {
  const [authors, setAuthors] = useState<Author[]>([]);
  const [categories, setCategories] = useState<Category[]>([]);

  useEffect(() => {
    getAuthors().then((authors) => setAuthors(authors));
    getCategories().then((categories) => setCategories(categories));
  }, []);

  const onFieldChange = useCallback(
    (field: keyof ProcessedVideo): React.ChangeEventHandler<{ value: string }> => ({ target: { value } }) => {
      // TODO validate

      onChange({
        ...video,
        [field]: value,
      });
    },
    [onChange, video]
  );

  return (
    <form noValidate autoComplete="off">
      <Grid container direction="column" spacing={4}>
        <Grid item>
          <TextField id="name" label="Name" value={video.name} onChange={onFieldChange('name')} fullWidth data-testid="name"/>
        </Grid>

        <Grid item>
          <TextField id="author" select label="Author" value={video.author} onChange={onFieldChange('author')} fullWidth data-testid="author">
            {authors.map((a) => (
              <MenuItem value={a.name} key={a.id}>
                {a.name}
              </MenuItem>
            ))}
          </TextField>
        </Grid>

        <Grid item>
          <TextField
            id="categories"
            select
            label="Categories"
            value={video.categories}
            SelectProps={{ multiple: true }}
            onChange={onFieldChange('categories')}
            fullWidth
            data-testid="categories">
            {categories.map((c) => (
              <MenuItem value={c.name} key={c.id}>
                {c.name}
              </MenuItem>
            ))}
          </TextField>
        </Grid>

        <Grid item>
          <Button color="primary" variant="contained" onClick={onSubmit} data-testid="submit">
            Submit
          </Button>
          <Button onClick={onCancel} data-testid="cancel">Cancel</Button>
        </Grid>
      </Grid>
    </form>
  );
};
