import { Button, Dialog, DialogActions, DialogContent, DialogTitle } from '@material-ui/core';
import React from 'react';
import { ProcessedVideo } from './interfaces';

interface DeleteDialogProps {
  open: boolean;
  video: ProcessedVideo | null;
  onDelete: () => void;
  onCancel: () => void;
}

export const DeleteDialog: React.FC<DeleteDialogProps> = ({ open, video, onCancel, onDelete }) => {
  if (!video) {
    return null;
  }
  return (
    <Dialog aria-labelledby="confirmation-dialog-title" open={open} data-testid="delete-dialog">
      <DialogTitle id="confirmation-dialog-title">
        Delete <em>«{video.name}»</em> by {video.author}?
      </DialogTitle>
      <DialogContent dividers>Are you sure you want to delete this video?</DialogContent>
      <DialogActions>
        <Button onClick={onCancel} data-testid="cancel-delete">
          Cancel
        </Button>
        <Button onClick={onDelete} color="secondary" data-testid="confirm-delete">
          Delete
        </Button>
      </DialogActions>
    </Dialog>
  );
};
