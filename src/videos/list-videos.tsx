import { Button, Grid, TextField } from '@material-ui/core';
import { Refresh } from '@material-ui/icons';
import React, { useCallback, useEffect, useState } from 'react';
import { useHistory } from 'react-router-dom';
import { DeleteDialog } from './components/delete-dialog';
import { VideosTable } from './components/videos-table';
import { ProcessedVideo } from './interfaces';
import { deleteVideo, getVideos } from './services/videos';

export const ListVideos: React.FunctionComponent = () => {
  const history = useHistory();
  const [query, setQuery] = useState<string>('');
  const [videos, setVideos] = useState<ProcessedVideo[]>([]);
  const [filteredVideos, setFilteredVideos] = useState<ProcessedVideo[]>([]);
  const [isDeleteDialogOpen, setDeleteDialogOpen] = useState<boolean>(false);
  const [videoToDelete, setVideoToDelete] = useState<ProcessedVideo | null>(null);

  const onQueryChange = ({ target: { value } }: React.ChangeEvent<{ value: string }>) => {
    setQuery(value);
  };

  const refresh = async () => {
    const videos = await getVideos();
    setVideos(videos);
  };

  useEffect(() => {
    setFilteredVideos(
      videos.filter(
        (v) =>
          !query ||
          Object.values(v)
            .flat()
            .map((s) => s?.toString().toLowerCase())
            .some((s) => s?.includes(query))
      )
    );
  }, [query, videos]);

  const onClick = () => {
    history.push('/videos/new');
  };

  const showDeleteDialog = (video: ProcessedVideo) => {
    setVideoToDelete(video);
    setDeleteDialogOpen(true);
  };

  const hideDeleteDialog = () => {
    setDeleteDialogOpen(false);
  };

  const onDeleteVideo = useCallback(async () => {
    if (!videoToDelete) {
      return;
    }
    hideDeleteDialog();
    await deleteVideo(videoToDelete);
    await refresh();
  }, [videoToDelete]);

  useEffect(() => {
    refresh();
  }, []);

  return (
    <>
      <Grid container direction="row" spacing={1}>
        <Grid item xs>
          <h2>Videos</h2>
        </Grid>
        <Grid item>
          <TextField aria-label="Search" placeholder="Search" type="search" color="primary" value={query} onChange={onQueryChange} data-testid="search" />
        </Grid>
        <Grid item>
          <Button variant="outlined" aria-label="refresh videos list" onClick={refresh} data-testid="refresh-list">
            <Refresh />
          </Button>
        </Grid>
        <Grid item>
          <Button color="primary" variant="outlined" onClick={onClick} data-testid="add-video">
            Add video
          </Button>
        </Grid>
      </Grid>
      <VideosTable videos={filteredVideos} onDeleteVideo={showDeleteDialog} />
      <DeleteDialog open={isDeleteDialogOpen} video={videoToDelete} onDelete={onDeleteVideo} onCancel={hideDeleteDialog} />
    </>
  );
};
