import { AppBar, makeStyles, Toolbar, Typography } from '@material-ui/core';
import React from 'react';
import { Link } from 'react-router-dom';

const useStyles = makeStyles((theme) => ({
  title: {
    flexGrow: 1,
    fontSize: '1.5rem',
    textDecoration: 'none',
    color: 'inherit',
  },
}));

export const TopNav = () => {
  const classes = useStyles();

  return (
    <AppBar position="static">
      <Toolbar>
        <Typography variant="h1" component={Link} to="/" className={classes.title}>
          Video manager
        </Typography>
      </Toolbar>
    </AppBar>
  );
};
