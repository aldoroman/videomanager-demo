import { Box, Container } from '@material-ui/core';
import React from 'react';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';
import { TopNav } from './components/top-nav';
import { AddVideo } from './videos/add-video';
import { EditVideo } from './videos/edit-video';
import { ListVideos } from './videos/list-videos';

const App: React.FC = () => {
  return (
    <Router>
      <TopNav />
      <Container>
        <Box padding="2rem 0">
          <Switch>
            <Route path="/videos/new">
              <AddVideo />
            </Route>
            <Route path="/videos/:id">
              <EditVideo />
            </Route>
            <Route path="/">
              <ListVideos />
            </Route>
          </Switch>
        </Box>
      </Container>
    </Router>
  );
};

export default App;
